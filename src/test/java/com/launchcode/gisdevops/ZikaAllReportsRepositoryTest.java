package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.ZikaAllReportsRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.ZikaAllReports;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ZikaAllReportsRepositoryTest {


    @Autowired
    private ZikaAllReportsRepository zikaAllReportsRepository;

    @Autowired
    private EntityManager entityManager;

    @Before
    public void setup(){
        zikaAllReportsRepository.deleteAll();
    }

    @Test
    public void findBySrcReturnsMatchingSrc() {
        Date date = new Date();
        ZikaAllReports zikaReport = zikaAllReportsRepository.save(
            new ZikaAllReports(date,
                    "location",
                    "location cleaned",
                    "location type",
                    "data field",
                    "data field code",
                    "time period",
                    "time period type",
                    1.0F,
                    "unit" )
        );

        entityManager.persist(zikaReport);
        entityManager.flush();

        List<ZikaAllReports> foundZikaReport = zikaAllReportsRepository.findByLocation(zikaReport.getLocation());

        assertEquals(1, foundZikaReport.size());

    }

}
