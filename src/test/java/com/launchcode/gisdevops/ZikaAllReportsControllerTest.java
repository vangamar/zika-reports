package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.ZikaAllReportsRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.ZikaAllReports;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ZikaAllReportsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ZikaAllReportsRepository zikaAllReportsRepository;

    @Before
    public void setup(){
        zikaAllReportsRepository.deleteAll();
    }

    @After
    public void tearDown(){
        zikaAllReportsRepository.deleteAll();
    }

    @Test
    public void zikaReportPathWorks() throws Exception {
        this.mockMvc.perform(get("/zika_report/")).andExpect(status().isOk());
        this.mockMvc.perform(get("/zika_report")).andExpect(status().isOk());
    }

    @Test
    public void zikaReportsReturnsOneElement() throws Exception {
        Date date = new Date();
        ZikaAllReports zikaReport = zikaAllReportsRepository.save(
                new ZikaAllReports(date,
                        "location",
                        "location cleaned",
                        "location type",
                        "data field",
                        "data field code",
                        "time period",
                        "time period type",
                        1.0F,
                        "unit" )
        );

        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features"), hasSize(1)));
    }

    @Test
    public void noZikaReportsMeansEmptyResults() throws Exception {
        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.type"), containsString("FeatureCollection")))
                .andExpect(jsonPath(("$.features"), hasSize(0)));
    }

    @Test
    public void allAttributesAreOnTheGeoJSON() throws Exception {
        Date date = new Date();
        ZikaAllReports zikaReport = zikaAllReportsRepository.save(
                new ZikaAllReports(date,
                        "location",
                        "location cleaned",
                        "location type",
                        "data field",
                        "data field code",
                        "time period",
                        "time period type",
                        1.0F,
                        "unit" )
        );
        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features[0].properties.locationType"), containsString("String locationType")))
                .andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(51.1328010559082, .00001)));
                //.andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(13.767200469970703, .00001))); //seems to only work on latitude? this longitude doesn't work

    }

    @Test
    public void multipleZikaReportssGivesMultipleFeatures() throws Exception {
        Date date = new Date();
        ZikaAllReports zikaReport = zikaAllReportsRepository.save(
                new ZikaAllReports(date,
                        "location",
                        "location cleaned",
                        "location type",
                        "data field",
                        "data field code",
                        "time period",
                        "time period type",
                        1.0F,
                        "unit" )
        );
        zikaReport = zikaAllReportsRepository.save(
                new ZikaAllReports(date,
                        "location 2",
                        "location cleaned 2",
                        "location type 2",
                        "data field 2",
                        "data field code 2",
                        "time period 2",
                        "time period type 2",
                        2.0F,
                        "unit 2" )
        );

        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(2)));
    }

    @Test
    public void searchByZikaReportLocationAndId() throws Exception {
        Date date = new Date();
        ZikaAllReports zikaReport = zikaAllReportsRepository.save(
                new ZikaAllReports(date,
                        "location",
                        "location cleaned 2",
                        "location type",
                        "data field",
                        "data field code",
                        "time period",
                        "time period type",
                        1.0F,
                        "unit" )
        );
        //System.out.println("zikaReport ID = " + zikaReport.getId() );

        zikaReport = zikaAllReportsRepository.save(
                new ZikaAllReports(date,
                        "location 2",
                        "location cleaned 2",
                        "location type 2",
                        "data field 2",
                        "data field code 2",
                        "time period 2",
                        "time period type 2",
                        2.0F,
                        "unit 2" )
        );

        //System.out.println("zikaReport ID = " + zikaReport.getId() );
        this.mockMvc.perform(get("/zika_report?id=" + zikaReport.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(1)))
                .andExpect(jsonPath("$.features[0].properties.id", equalTo(zikaReport.getId().intValue())));
    }

}
