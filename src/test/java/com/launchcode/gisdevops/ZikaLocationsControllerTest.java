package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.ZikaLocationsRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.ZikaLocations;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ZikaLocationsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ZikaLocationsRepository zikaLocationsRepository;

    @Before
    public void setup(){
        zikaLocationsRepository.deleteAll();
    }

    @After
    public void tearDown(){
        zikaLocationsRepository.deleteAll();
    }

    @Test
    public void zikaReportPathWorks() throws Exception {
        this.mockMvc.perform(get("/zika_locations/")).andExpect(status().isOk());
        this.mockMvc.perform(get("/zika_locations")).andExpect(status().isOk());
    }


    @Test
    public void zikaReportsReturnsOneElement() throws Exception {
        //Date date = new Date();
        ZikaLocations zikaLocations = zikaLocationsRepository.save(
                // Have to remove 'SRID=4326;' from the MULTIPOLYGON string from the locations TSV file
                // otherwise wktToGeometry throws "Not a WKT string:" exception
                // (SRID is in there for PostGIS DB ingest)
                new ZikaLocations(
                        "Argentina", "Buenos Aires",
                        WktHelper.wktToGeometry("MULTIPOLYGON (((-58.15141677856445 -27.2689208984375, -57.72533798217768 -27.324899673461914, -57.510852813720646 -27.4389972686767, -57 -27.46483421325678, -56.85004806518549 -27.604213714599553, -56.598140716552734 -27.445108413696175, -56.37533569335926 -27.60158920288086, -56.28265380859375 -27.408508300781193, -56.05329513549805 -27.30630874633789, -56.0578727722168 -27.457826614379883, -55.85039901733393 -27.761585235595703, -55.82279586791992 -27.944175720214787, -55.61629104614252 -28.132150650024414, -55.77482986450195 -28.247774124145508, -55.66987991333002 -28.33030891418457, -55.69490432739258 -28.42241668701172, -55.874046325683594 -28.359375, -55.88801956176758 -28.479166030883732, -56.01074981689453 -28.50913238525385, -56.01544952392578 -28.607809066772404, -56.28425216674799 -28.791032791137695, -56.42478561401367 -29.072883605956918, -56.59727478027344 -29.126686096191406, -56.6756706237793 -29.32118797302246, -56.97092819213867 -29.637376785278263, -57.28284835815418 -29.819894790649414, -57.46672821044922 -30.115768432617188, -57.6414680480957 -30.18011474609375, -57.635391235351506 -30.33802413940424, -57.877586364746094 -30.51555824279785, -57.80221557617182 -30.760452270507812, -58.01610946655268 -30.597978591918945, -58.20350265502924 -30.294397354125863, -58.58065414428711 -30.165771484374943, -59.020603179931584 -30.21880531311035, -59.23936080932617 -30.36092758178711, -59.55445861816406 -30.334098815917912, -59.59590530395508 -30.425628662109375, -59.689186096191406 -30.28143882751465, -59.562961578369084 -30.0284366607666, -59.68010330200184 -29.830669403076115, -59.609138488769474 -29.403701782226506, -59.492942810058594 -29.225051879882812, -59.21342468261719 -29.061128616333008, -59.07714080810547 -28.644676208496037, -59.09724044799805 -28.21149826049799, -58.87082290649414 -28.032621383666992, -58.811077117919865 -27.778911590576115, -58.88720703124994 -27.482240676879883, -58.60635757446278 -27.2953262329101, -58.15141677856445 -27.2689208984375)))"))
        );

        this.mockMvc.perform(get("/zika_locations"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features"), hasSize(1)));
    }

    @Test
    public void noZikaLocationssMeansEmptyResults() throws Exception {
        this.mockMvc.perform(get("/zika_locations"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.type"), containsString("FeatureCollection")))
                .andExpect(jsonPath(("$.features"), hasSize(0)));
    }

    @Test
    public void allAttributesAreOnTheGeoJSON() throws Exception {
        //Date date = new Date();
        ZikaLocations zikaLocations = zikaLocationsRepository.save(
                new ZikaLocations(
                        "Argentina", "Buenos Aires",
                        WktHelper.wktToGeometry("MULTIPOLYGON (((-58.15141677856445 -27.2689208984375, -57.72533798217768 -27.324899673461914, -57.510852813720646 -27.4389972686767, -57 -27.46483421325678, -56.85004806518549 -27.604213714599553, -56.598140716552734 -27.445108413696175, -56.37533569335926 -27.60158920288086, -56.28265380859375 -27.408508300781193, -56.05329513549805 -27.30630874633789, -56.0578727722168 -27.457826614379883, -55.85039901733393 -27.761585235595703, -55.82279586791992 -27.944175720214787, -55.61629104614252 -28.132150650024414, -55.77482986450195 -28.247774124145508, -55.66987991333002 -28.33030891418457, -55.69490432739258 -28.42241668701172, -55.874046325683594 -28.359375, -55.88801956176758 -28.479166030883732, -56.01074981689453 -28.50913238525385, -56.01544952392578 -28.607809066772404, -56.28425216674799 -28.791032791137695, -56.42478561401367 -29.072883605956918, -56.59727478027344 -29.126686096191406, -56.6756706237793 -29.32118797302246, -56.97092819213867 -29.637376785278263, -57.28284835815418 -29.819894790649414, -57.46672821044922 -30.115768432617188, -57.6414680480957 -30.18011474609375, -57.635391235351506 -30.33802413940424, -57.877586364746094 -30.51555824279785, -57.80221557617182 -30.760452270507812, -58.01610946655268 -30.597978591918945, -58.20350265502924 -30.294397354125863, -58.58065414428711 -30.165771484374943, -59.020603179931584 -30.21880531311035, -59.23936080932617 -30.36092758178711, -59.55445861816406 -30.334098815917912, -59.59590530395508 -30.425628662109375, -59.689186096191406 -30.28143882751465, -59.562961578369084 -30.0284366607666, -59.68010330200184 -29.830669403076115, -59.609138488769474 -29.403701782226506, -59.492942810058594 -29.225051879882812, -59.21342468261719 -29.061128616333008, -59.07714080810547 -28.644676208496037, -59.09724044799805 -28.21149826049799, -58.87082290649414 -28.032621383666992, -58.811077117919865 -27.778911590576115, -58.88720703124994 -27.482240676879883, -58.60635757446278 -27.2953262329101, -58.15141677856445 -27.2689208984375)))"))
        );
        this.mockMvc.perform(get("/zika_locations"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features[0].type"), containsString("Feature")))
                .andExpect(jsonPath(("$.features[0].geometry.type"), containsString("MultiPolygon")))
                .andExpect(jsonPath(("$.features[0].properties.countryName"), containsString("Argentina")))
                .andExpect(jsonPath(("$.features[0].properties.stateName"), containsString("Buenos Aires")))
                //.andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(51.1328010559082, .00001)))
                //.andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(13.767200469970703, .00001))); //seems to only work on latitude? this longitude doesn't work
                ;
    }

//    @Test
//    public void multipleZikaReportssGivesMultipleFeatures() throws Exception {
//        Date date = new Date();
//        ZikaReport zikaReport = zikaReportRepository.save(
//                new ZikaReport(
//                        date, "String location", "String locationType", "String dataField",
//                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
//                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
//        );
//        zikaReport = zikaReportRepository.save(
//                new ZikaReport(
//                        date, "String location2", "String locationType2", "String dataField2",
//                        "String dataFieldCode2", "String timePeriod2", "String timePeriodType2",
//                        12, "String unit2", WktHelper.wktToGeometry("POINT(14.767200469970703 52.1328010559082)"))
//        );
//        this.mockMvc.perform(get("/zika_locations"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features", hasSize(2)));
//    }
//
//    @Test
//    public void searchByZikaReportLocationAndId() throws Exception {
//        Date date = new Date();
//        ZikaReport zikaReport = zikaReportRepository.save(
//                new ZikaReport(
//                        date, "String location", "String locationType", "String dataField",
//                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
//                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
//        );
//        //System.out.println("zikaReport ID = " + zikaReport.getId() );
//        zikaReport = zikaReportRepository.save(
//                new ZikaReport(
//                        date, "String location2", "String locationType2", "String dataField2",
//                        "String dataFieldCode2", "String timePeriod2", "String timePeriodType2",
//                        14, "String unit2", WktHelper.wktToGeometry("POINT(14.767200469970703 52.1328010559082)"))
//        );
//        //System.out.println("zikaReport ID = " + zikaReport.getId() );
//        this.mockMvc.perform(get("/zika_report?id=" + zikaReport.getId().toString()))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features", hasSize(1)))
//                .andExpect(jsonPath("$.features[0].properties.id", equalTo(zikaReport.getId().intValue())));
//    }

}
