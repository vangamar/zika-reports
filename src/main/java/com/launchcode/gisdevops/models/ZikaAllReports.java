package com.launchcode.gisdevops.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ZikaAllReports {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date reportDate;
    private String location;
    private String locationCleaned;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private Float value;
    private String unit;
    // private Geometry locationGeometry;  // this will be a join, in controller not DB itself, data not suitable for a DB level join

    @ManyToOne
    private ZikaLocations state;

    public ZikaAllReports() {
    }

    public ZikaAllReports(Date reportDate, String location, String locationCleaned, String locationType, String dataField,
                      String dataFieldCode, String timePeriod, String timePeriodType,
                      Float value, String unit) {
        this.reportDate = reportDate;
        this.location = location;
        this.locationCleaned = locationCleaned;
        this.locationType = locationType;
        this.dataField = dataField;
        this.dataFieldCode = dataFieldCode;
        this.timePeriod = timePeriod;
        this.timePeriodType = timePeriodType;
        this.value = value;
        this.unit = unit;
        //this.locationGeometry = locationGeometry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationCleaned() {
        return locationCleaned;
    }

    public void setLocationCleaned(String locationCleaned) {
        this.locationCleaned = locationCleaned;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public ZikaLocations getState() {
        return state;
    }

    public void setState(ZikaLocations state) {
        this.state = state;
    }
}
