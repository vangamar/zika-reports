package com.launchcode.gisdevops.models;

import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ZikaLocations {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String countryName;
    private String stateName;

    @OneToMany
    @JoinColumn(name = "state_id")
    private List<ZikaAllReports> reports = new ArrayList<>();

    private Geometry geometry;

    public ZikaLocations() {
    }


    //(Integer id_0, String iso, String name_0, Integer id_1, String name_1, String hasc_1, Integer ccn_1, String cca_1, String type_1, String engtype_1, String nl_name_1, String varname_1, Geometry geom)

    public ZikaLocations(String countryName, String stateName, Geometry geometry) {

        this.countryName = countryName;
        this.stateName = stateName;
        this.geometry = geometry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public List<ZikaAllReports> getReports() {
        return reports;
    }

    public void setReports(List<ZikaAllReports> reports) {
        this.reports = reports;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
