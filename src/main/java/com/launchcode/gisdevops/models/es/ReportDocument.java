package com.launchcode.gisdevops.models.es;

import com.launchcode.gisdevops.models.ZikaAllReports;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Document(indexName = "#{esConfig.indexName}", type = "items")
public class ReportDocument {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    private String reportDate;
    private String location;
    private String locationCleaned;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private Float value;
    private String unit;
    private Long state_id;

    public ReportDocument() {}

    public ReportDocument(ZikaAllReports report) {

        this.reportDate = report.getReportDate().toString();
        this.location = report.getLocation();
        this.locationCleaned = report.getLocationCleaned();
        this.locationType = report.getLocationType();
        this.dataField = report.getDataField();
        this.dataFieldCode = report.getDataFieldCode();
        this.timePeriod = report.getTimePeriod();
        this.timePeriodType = report.getTimePeriodType();
        this.value = report.getValue();
        this.unit = report.getUnit();

        boolean goodState = report.getState() != null ? true : false;
        this.state_id =  goodState ?  report.getState().getId() : -1;
    }

    // Getters and setters omitted


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationCleaned() {
        return locationCleaned;
    }

    public void setLocationCleaned(String locationCleaned) {
        this.locationCleaned = locationCleaned;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getState_id() {
        return state_id;
    }

    public void setState_id(Long state_id) {
        this.state_id = state_id;
    }
}
