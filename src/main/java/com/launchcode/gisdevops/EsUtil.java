package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.ReportDocumentRepository;
import com.launchcode.gisdevops.data.ZikaAllReportsRepository;
import com.launchcode.gisdevops.models.ZikaAllReports;
import com.launchcode.gisdevops.models.es.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EsUtil {

    @Autowired
    private ZikaAllReportsRepository zikaAllReportsRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    public void refresh() {
        reportDocumentRepository.deleteAll();
        List<ReportDocument> reportDocuments = new ArrayList<>();
        for(ZikaAllReports report : zikaAllReportsRepository.findAll()) {
            ReportDocument reportDocument = new ReportDocument(report);
            reportDocuments.add(reportDocument);
        }
        reportDocumentRepository.saveAll(reportDocuments);
    }
}
