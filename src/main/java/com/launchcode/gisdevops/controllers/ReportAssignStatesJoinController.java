package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.data.ReportDocumentRepository;
import com.launchcode.gisdevops.data.ZikaAllReportsRepository;
import com.launchcode.gisdevops.data.ZikaLocationsRepository;
import com.launchcode.gisdevops.models.ZikaAllReports;
import com.launchcode.gisdevops.models.ZikaLocations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/api/report")
public class ReportAssignStatesJoinController {


    @Autowired
    private ZikaAllReportsRepository reportRepository;

    @Autowired
    private ZikaLocationsRepository locationRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    // Only needs to be ran once to populate the requisite join columns
    @PostMapping(value = "/assignStates")
    public ResponseEntity<String> assignStates() {

        List<ZikaLocations> locations = locationRepository.findAll();

        for (ZikaLocations location : locations) {
            String countryPart = location.getCountryName().replace(" ", "_");
            String statePart = location.getStateName().replace(" ", "_");
            List<ZikaAllReports> matchingReports = reportRepository.findByLocationStartingWithIgnoreCase(countryPart + "-" + statePart);
            for (ZikaAllReports report : matchingReports) {
                report.setState(location);
            }
            reportRepository.saveAll(matchingReports);
        }

        return new ResponseEntity<>("Complete", HttpStatus.OK);
    }

}