package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.data.ZikaLocationsRepository;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.models.ZikaLocations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/zika_locations")
public class ZikaLocationsController {

    @Autowired
    private ZikaLocationsRepository zikaLocationsRepository;

    @RequestMapping(value = {"/", ""})
    @ResponseBody
    public FeatureCollection getZikaLocations(@RequestParam(name="id") Optional<Long> id)  {
        List<ZikaLocations> zikaLocations;
        if(id.isPresent() && !id.toString().isEmpty()) {
            zikaLocations = new ArrayList<ZikaLocations>();
            zikaLocations.add(zikaLocationsRepository.findById(id.get()).get());
        } else {
            zikaLocations = zikaLocationsRepository.findAll();
        }

        if(zikaLocations.isEmpty()) { return new FeatureCollection(); }

        FeatureCollection featureCollection = new FeatureCollection();
        for (ZikaLocations zikaLoc : zikaLocations) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("countryName", zikaLoc.getCountryName());
            properties.put("stateName", zikaLoc.getStateName());
            featureCollection.addFeature(new Feature(zikaLoc.getGeometry(), properties));
        }
        return featureCollection;


    }
}
