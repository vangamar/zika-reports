package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.data.ZikaAllReportsRepository;
import com.launchcode.gisdevops.data.ZikaLocationsRepository;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.models.ZikaAllReports;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;

import java.util.*;

@RestController
@RequestMapping(value = "/zika_all_reports")
public class ZikaAllReportsRestController {

    @Autowired
    private ZikaAllReportsRepository zikaAllReportsRepository;
    @Autowired
    private ZikaLocationsRepository zikaLocationsRepository;

    @GetMapping(value = {"/", ""})
    public ResponseEntity getZikaAllReports(@RequestParam(name="id") Optional<Long> id,
                                            @RequestParam(name="date") Optional<String> dateStr) throws Exception {
        List<ZikaAllReports> zikaAllReports;

        if(id.isPresent() && !id.toString().isEmpty()) {
            zikaAllReports = new ArrayList<ZikaAllReports>();
            zikaAllReports.add(zikaAllReportsRepository.findById(id.get()).get());
        }
        else if (dateStr.isPresent() && !dateStr.toString().isEmpty()) {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr.get());
            zikaAllReports = zikaAllReportsRepository.findByReportDate(date);
        }
        else {
            // Just get first one hundred as a default to show it's working
            zikaAllReports = new ArrayList<ZikaAllReports>();
            for (Long i=1L; i<=100L; i++) {
                boolean isPresent = zikaAllReportsRepository.findById(i).isPresent();
                if (isPresent) {
                    zikaAllReports.add(zikaAllReportsRepository.findById(i).get());
                } else {
                    break;
                }

            }
        }

        if(zikaAllReports.isEmpty()) {
            return new ResponseEntity(zikaAllReports, HttpStatus.OK);
        }

        FeatureCollection featureCollection = new FeatureCollection();
        WKTReader wkt = new WKTReader();
        Geometry bermudaTriangleGeom;
        try {
            // Use a line polygon of the Bermuda Triangle as the default if no join was found
            bermudaTriangleGeom = wkt.read("LINESTRING(-80.10670906394921 25.71246613161172,-66.04420906394921 18.38752086805181,-64.94557625144921 32.09341455052285,-80.10670906394921 25.71246613161172)");
        } catch (ParseException e) {
            throw new RuntimeException("Not a WKT string in controller hard coded string");
        }


        for (ZikaAllReports zikaRep : zikaAllReports) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("location", zikaRep.getLocation());
            properties.put("reportDate", zikaRep.getReportDate());
            properties.put("locationType", zikaRep.getLocationType());
            properties.put("dataField", zikaRep.getDataField());
            properties.put("dataFieldCode", zikaRep.getDataFieldCode());
            properties.put("timePeriod", zikaRep.getTimePeriod());
            properties.put("timePeriodType", zikaRep.getTimePeriodType());
            properties.put("value", zikaRep.getValue());
            properties.put("unit", zikaRep.getUnit());


            // deleted the super slow and bugged manual string names join and refactored to
            // use the new 'state' column we added in a DB join (in ReportAssignStatesJoinController)
            boolean goodGeom = false;
            boolean goodState = zikaRep.getState() != null ? true : false;
            if (goodState) {
                goodGeom =  zikaRep.getState().getGeometry() != null ? true : false;
            }

            if (goodGeom) {
                featureCollection.addFeature(
                    new Feature(zikaRep.getState().getGeometry(), properties));
            }
            else {
                // use dummy placeholder geometry if no match was found
                featureCollection.addFeature(new Feature(bermudaTriangleGeom, properties));
            }
        }
        return new ResponseEntity(featureCollection, HttpStatus.OK);


    }

    //Returns an ordered unique date list with report counts next to each date
    @GetMapping(value = {"/get_date_list", "/get_date_list/"})
    public ResponseEntity getZikaAllReportsDateList()  {
        // List<String> dateList = new ArrayList<String>();
        List<String> dateList =  zikaAllReportsRepository.findAllDatesOrderedAsc();
        return new ResponseEntity(dateList, HttpStatus.OK);
    }

// end of class
}

//    Saving ItemDocuments
//    While we have code in place to carry out searches in Elasticsearch via our API, there are not any documents in the ES index quite yet.
//
//        Within ItemController and ItemRestController, let’s save a new ReportDocument every time we create a new Item.
//
//        We previously saved and returned a new Item like this:
//
//        itemRepository.save(item);
//        Now, however, we must also save an ReportDocument for each newly-created item:
//
//        Item savedItem = itemRepository.save(item);
//        itemDocumentRepository.save(new ReportDocument(savedItem));
//        You will need to @Autowire a ReportDocumentRepository in each controller that needs access to Elasticsearch.

