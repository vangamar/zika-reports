package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.data.ReportDocumentRepository;
import com.launchcode.gisdevops.data.ZikaLocationsRepository;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.models.ZikaLocations;
import com.launchcode.gisdevops.models.es.ReportDocument;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping(value = "/api/items")
public class ReportDocumentController {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private ZikaLocationsRepository zikaLocationsRepository;



    //TODO: upgrade API usage, fuzzy search kinda working, apparently fuzzyQuery is being deprecated and sucks?
    @GetMapping(value = "search")
    public ResponseEntity search(@RequestParam String q) {
        q = q.toLowerCase();
        q = q.replaceAll(" ", "");
        q = q.replaceAll("-", "");
        q = q.replaceAll("_", "");

        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("locationCleaned", q);
        List<ReportDocument> results = new ArrayList<>();
        Iterator<ReportDocument> iterator = reportDocumentRepository.search(fuzzyQueryBuilder).iterator();

        FeatureCollection featureCollection = new FeatureCollection();
        WKTReader wkt = new WKTReader();
        Geometry bermudaTriangleGeom;
        try {
            // Use a line polygon of the Bermuda Triangle as the default if no join was found
            bermudaTriangleGeom = wkt.read("LINESTRING(-80.10670906394921 25.71246613161172,-66.04420906394921 18.38752086805181,-64.94557625144921 32.09341455052285,-80.10670906394921 25.71246613161172)");
        } catch (ParseException e) {
            throw new RuntimeException("Not a WKT string in controller hard coded string");
        }

        ZikaLocations zikaLoc;
        ReportDocument repDoc;
        long state_id;
        while(iterator.hasNext()) {
            repDoc = iterator.next();
            try {
                state_id = repDoc.getState_id();

                HashMap<String, Object> properties = new HashMap<>();
                properties.put("location", repDoc.getLocation());
                properties.put("reportDate", repDoc.getReportDate());
                properties.put("locationType", repDoc.getLocationType());
                properties.put("dataField", repDoc.getDataField());
                properties.put("dataFieldCode", repDoc.getDataFieldCode());
                properties.put("timePeriod", repDoc.getTimePeriod());
                properties.put("timePeriodType", repDoc.getTimePeriodType());
                properties.put("value", repDoc.getValue());
                properties.put("unit", repDoc.getUnit());

                if (zikaLocationsRepository.findById(state_id).isPresent()) {
                    zikaLoc = zikaLocationsRepository.findById(state_id).get();
                    featureCollection.addFeature(new Feature(zikaLoc.getGeometry(), properties));
                } else {
                    // use dummy placeholder geometry if no match was found
                    featureCollection.addFeature(new Feature(bermudaTriangleGeom, properties));
                }
            }
            catch (Exception e) {
                System.out.println(e.toString());
            }

        }
        return new ResponseEntity(featureCollection, HttpStatus.OK);
    }



}