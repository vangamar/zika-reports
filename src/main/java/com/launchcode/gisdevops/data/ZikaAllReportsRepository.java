package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.ZikaAllReports;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface ZikaAllReportsRepository extends JpaRepository<ZikaAllReports, Long> {

//    @Query(
//            value = "SELECT * FROM USERS u WHERE u.status = 1",
//            nativeQuery = true)
//    Collection<User> findAllActiveUsersNative();

    @Query(value = "SELECT DISTINCT report_date, COUNT(location) FROM zika_all_reports GROUP BY report_date ORDER BY report_date ASC", nativeQuery = true)
    public List<String> findAllDatesOrderedAsc();

    public List<ZikaAllReports> findByLocationStartingWithIgnoreCase(String location);

    public List<ZikaAllReports> findByReportDate(Date date);
    public List<ZikaAllReports> findByLocation(String location);
    public List<ZikaAllReports> findByState(String location);
    public Optional<ZikaAllReports> findById(Long id);


}
