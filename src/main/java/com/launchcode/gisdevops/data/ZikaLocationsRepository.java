package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.ZikaLocations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface ZikaLocationsRepository extends JpaRepository<ZikaLocations, Long> {
    public Optional<ZikaLocations> findById(Long id);

    // code and data was refactored to use separate country and state name
    // Hybernate magic: using "...And..." in findBy will search on  both columns
    public List<ZikaLocations> findByCountryNameAndStateName(String countryName, String stateName);
}
