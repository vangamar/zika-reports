package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.es.ReportDocument;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Spliterator;

public interface ReportDocumentRepository
        extends ElasticsearchRepository<ReportDocument, String> {

    Iterable<ReportDocument> search(QueryBuilder queryBuilder);

}
