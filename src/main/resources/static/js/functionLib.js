let G_oneDayMillsecondsFixup = 0;

function populateDateListSelectBox(data) {
    $('#reportDateList').empty();
    $('#debug').empty();

    // BANDAIDE!!!!!!!!!
    // When deployed as a JAR file service with systemctl on AWS ubuntu server, the day is returned as one day less
    // for every date returned to this function than when running in IntelliJ on dev machines
    let date = new Date(data[0][0]);
    if (date.getDate() == 27) {
        G_oneDayMillsecondsFixup = 86400000;
    }
    // BANDAIDE!!!!!!!!!

    let outStr = '';
    for (let i=0; i<data.length; i++) {
        //console.log(`raw date from Java was: '${data[i][0]}'`);
        let date = new Date(data[i][0]);
        date.setTime( date.getTime() + G_oneDayMillsecondsFixup); // BANDAIDE!!!!!!!!!
        let month = date.getMonth()+1;
        let day = date.getDate();
        month = (month < 10) ? `0${month}` : `${month}`;
        day = (day < 10) ? `0${day}` : `${day}`;
        let dateStr = `${date.getFullYear()}-${month}-${day}`;
        outStr += `<option value="${dateStr}">${dateStr} | ${data[i][1]} reports</option>`;
    }
    $('#reportDateList').append(outStr);

}

function buildClickedCDClocationReport(CDClocationReport, feature) {
    if (! feature.get('countryName')) {
        return null;
    }

    CDClocationReport[CDClocationReport.length] = {
        countryName:   feature.get('countryName'),
        stateName:     feature.get('stateName')
    }
    return CDClocationReport;
}

function buildClickedReports(reports, feature) {
    let date = new Date(feature.get('reportDate')); // comes out of feature.get as a string representing milliseconds since epic or something similar
    date.setTime( date.getTime() + G_oneDayMillsecondsFixup); // BANDAIDE!!!!!!!!!
    date = date.toDateString();

    if (! feature.get('location')) {
        return false;
    }

    if (reports.length == 0) {
        reports[0] = {
            totalCases:   feature.get('value'),
            location:     feature.get('location'),
            locationType: feature.get('locationType'),
            reportEntry: [
                {
                    reportDate:     date,
                    dataField:      feature.get('dataField'),
                    dataFieldCode:  feature.get('dataFieldCode'),
                    timePeriod:     feature.get('timePeriod'),
                    timePeriodType: feature.get('timePeriodType'),
                    value:          feature.get('value'),
                    unit:           feature.get('unit')
                }
            ]
        };
    }
    else {
        let similarIndex = -1;
        for (let i=0; i<reports.length; i++) {
            if (reports[i].location == feature.get('location') && reports[i].locationType == feature.get('locationType')) {
                similarIndex = i;
            }
        }

        if (similarIndex > -1) {
            let len = reports[similarIndex].reportEntry.length;
            reports[similarIndex].reportEntry[len] = {
                reportDate:     date,
                dataField:      feature.get('dataField'),
                dataFieldCode:  feature.get('dataFieldCode'),
                timePeriod:     feature.get('timePeriod'),
                timePeriodType: feature.get('timePeriodType'),
                value:          feature.get('value'),
                unit:           feature.get('unit')
            };
            reports[0].totalCases += feature.get('value');
       }
        else {
            let len = reports.length;
            reports[len] = {
                location:     feature.get('location'),
                locationType: feature.get('locationType'),
                reportEntry: [
                    {
                        reportDate:     date,
                        dataField:      feature.get('dataField'),
                        dataFieldCode:  feature.get('dataFieldCode'),
                        timePeriod:     feature.get('timePeriod'),
                        timePeriodType: feature.get('timePeriodType'),
                        value:          feature.get('value'),
                        unit:           feature.get('unit')
                    }
                ]
            };
            reports[0].totalCases += feature.get('value');
        }
    }
    return true;
}

var G_dialog;

function linkLocationToFuzzySrch(location) {
    //alert(`onclick event was passed location: ${location}`);
    G_dialog.dialog( "close" );
    document.getElementById('fuzzySearchText').value = location;

}

function displayClickedCDClocationReport(CDClocationReport, dialog) {
    let outStr = "";
    for (let i=0; i<CDClocationReport.length; i++) {
        let countryName = CDClocationReport[i].countryName;
        let stateName = CDClocationReport[i].stateName;
        outStr +=  `<div style="margin-bottom: 10px">
                       <table style="color: #FEFEFE; background-color: #1010EE; width:100%">
                          <tr><td>Location:</td><td class="locationLink" onclick="linkLocationToFuzzySrch('${countryName}-${stateName}')">${countryName}-${stateName}</td></tr>
                       </table>
                    </div>
                    <div style="margin-left: 30px; margin-bottom: 10px; font-size: 70%">
                        <b>Location:</b>
                        <table style="margin-left: 20px; border: 1px solid #D7D7D7">
                            <tr><td>Country Name:</td><td>${countryName}</td></tr>
                            <tr><td>State Name:</td><td>${stateName}</td></tr>
                        </table>
                    </div>
                    `;
    }

     dialog.dialog({title: `Unjoined (unmatched Country/State name) from CDC locations CSV file`});
     //$('#zika_reports').append(outStr);
     document.getElementById('zika_reports').innerHTML = outStr;
     dialog.dialog( "open" );
}

function displayClickedReports(reports, dialog) {

    //$('#zika_reports').empty();
    let outStr = "";
    let ttlRep = 0;
    let ttlCases = 0;

    if (reports.length > 0) {
        ttlCases = reports[0].totalCases;
        for (let i=0; i<reports.length; i++) {
            outStr += `<div style="margin-bottom: 10px">
                           <table style="color: #FEFEFE; background-color: #1010EE; width:100%">
                              <tr><td>Location:</td><td class="locationLink" onclick="linkLocationToFuzzySrch('${reports[i].location}')">${reports[i].location}</td></tr>
                              <tr><td>Location Type:</td><td>${reports[i].locationType}</td></tr>
                           </table>
                       </div>
                       `;
            for (let j=0; j<reports[i].reportEntry.length; j++) {
                ttlRep++;
                outStr += `
                    <div style="margin-left: 30px; margin-bottom: 10px; font-size: 70%">
                        <b>Report ${ttlRep}:</b>
                        <table style="margin-left: 20px; border: 1px solid #D7D7D7">
                            <tr><td>Report Date:</td><td>${reports[i].reportEntry[j].reportDate}</td></tr>
                            <tr><td>Data Field:</td><td>${reports[i].reportEntry[j].dataField}</td></tr>
                            <tr><td>Data Field Code:</td><td>${reports[i].reportEntry[j].dataFieldCode}</td></tr>
                            <tr><td>Time Period:</td><td>${reports[i].reportEntry[j].timePeriod}</td></tr>
                            <tr><td>Time Period Type:</td><td>${reports[i].reportEntry[j].timePeriodType}</td></tr>
                            <tr><td>Number of Cases:</td><td>${reports[i].reportEntry[j].value}</td></tr>
                            <tr><td>Unit:</td><td>${reports[i].reportEntry[j].unit}</td></tr>
                        </table>
                    </div>
                `;
            }
            outStr += ``;
        }
    }
    else {
        outStr = `<h3>No Zika reports<br />found at click point.</h3>`;
    }

    dialog.dialog({title: `${ttlRep} Reports Selected,  ${ttlCases} Zika cases reported`});
    //$('#zika_reports').append(outStr);
    document.getElementById('zika_reports').innerHTML = outStr;
    dialog.dialog( "open" );

}