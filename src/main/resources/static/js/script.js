

$(document).ready(function () {

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
    var dialog;
    $body = $("body");
//    var dialog, form,
//
//      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
//      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
//      name = $( "#name" ),
//      email = $( "#email" ),
//      password = $( "#password" ),
//      allFields = $( [] ).add( name ).add( email ).add( password ),
//      tips = $( ".validateTips" );

//    function updateTips( t ) {
//      tips
//        .text( t )
//        .addClass( "ui-state-highlight" );
//      setTimeout(function() {
//        tips.removeClass( "ui-state-highlight", 1500 );
//      }, 500 );
//    }
//
//    function checkLength( o, n, min, max ) {
//      if ( o.val().length > max || o.val().length < min ) {
//        o.addClass( "ui-state-error" );
//        updateTips( "Length of " + n + " must be between " +
//          min + " and " + max + "." );
//        return false;
//      } else {
//        return true;
//      }
//    }
//
//    function checkRegexp( o, regexp, n ) {
//      if ( !( regexp.test( o.val() ) ) ) {
//        o.addClass( "ui-state-error" );
//        updateTips( n );
//        return false;
//      } else {
//        return true;
//      }
//    }


    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 600,
      width: 550,
      modal: false,
      buttons: {
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
//      close: function() {
//        form[ 0 ].reset();
//        allFields.removeClass( "ui-state-error" );
//      }
    });
    G_dialog = dialog;

//    form = dialog.find( "form" ).on( "submit", function( event ) {
//      event.preventDefault();
//      addUser();
//    });

//    $( "#create-user" ).button().on( "click", function() {
//      dialog.dialog( "open" );
//    });
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

      //var bounds = [-118.36888885498, -33.7470817565918,
      //              -28.84694480896, 32.7180404663086];

      var mousePositionControl = new ol.control.MousePosition({
        className: 'custom-mouse-position',
        target: document.getElementById('location'),
        //projection: 'EPSG:4326',
        coordinateFormat: ol.coordinate.createStringXY(5),
        undefinedHTML: '&nbsp;'
      });
      var projection = new ol.proj.Projection({
          code: 'EPSG:4326',
          units: 'degrees',
          axisOrientation: 'neu',
          global: true
      });
      mousePositionControl.setProjection(projection);      //(ol.proj.get('EPSG:4326'));

    //Define style, source, and layer to show Route features
    const lineStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'green', width: 1})
    });
    const lineStyleFunction = function(feature) {
        return lineStyle;
    }

    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    // build a starting zika locations background polygon layer and add to startup map
    const zikaBaseStatePolyStyle = new ol.style.Style({
            fill: new ol.style.Fill({
               color: 'rgba(8, 8, 255, 0.1)',
           }),
           stroke: new ol.style.Stroke({
               color: '#0A0A0A',
               width: 1,
           })
    });
    let zikaBaseLocationsSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: '/zika_locations/'
        //url: '/zika_all_reports?date=2016-02-03'
        //url: '/api/items/search?q=Brazil Rio Grande do Sul'
    });
    let zikaBaseLocationsLayer = new ol.layer.Vector({
        source: zikaBaseLocationsSource,
        title: "zikaBaseLocations",
        name: "zikaBaseLocations",
        style: zikaBaseStatePolyStyle
    });

    // Add GeoSvr states layer as WMS
    var zikaStatesGeoSvrParams = {'LAYERS': 'LC:states_with_cases_by_date',
       'TILED': true
    };
    var zikaStatesGeoSvrLayer = new ol.layer.Tile({
       source: new ol.source.TileWMS(({
          url: 'http://geosvr-host:8989/geoserver/wms',
          params: zikaStatesGeoSvrParams
       }))
    });


//      var map = new ol.Map({
//        controls: ol.control.defaults({
//          attribution: false
//        }).extend([mousePositionControl]),
//        target: 'map',
//        layers: [
//          untiled,
//          tiled
//        ],
//        view: new ol.View({
//           projection: projection
//        })
//      });
    const map = new ol.Map({
        controls: ol.control.defaults({
          attribution: false
        }).extend([mousePositionControl]),
        target: 'mapPlaceholder',
        layers: [
            osmLayer
            //zikaBaseLocationsLayer
            //zikaStatesGeoSvrLayer
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([-82, 23]),
            zoom: 4
        })
    });

    //map.addLayer(osmLayer);
    map.addLayer(zikaBaseLocationsLayer);
    map.addLayer(zikaStatesGeoSvrLayer);

    map.getView().on('change:resolution', function(evt) {
        var resolution = evt.target.get('resolution');
        var units = map.getView().getProjection().getUnits();
        var dpi = 25.4 / 0.28;
        var mpu = ol.proj.METERS_PER_UNIT[units];
        var scale = resolution * mpu * 39.37 * dpi;
        if (scale >= 9500 && scale <= 950000) {
            scale = Math.round(scale / 1000) + "K";
        } else if (scale >= 950000) {
            scale = Math.round(scale / 1000000) + "M";
        } else {
            scale = Math.round(scale);
        }
        document.getElementById('scale').innerHTML = "Scale = 1 : " + scale;
    });
//    map.getView().fit(bounds, map.getSize());

    //Define style, source, and layer to show Airport features
    const pointStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            fill: null,
            stroke: new ol.style.Stroke({color: 'red', width: 1})
        })
    });

    const zikaFoundStatePolyStyle = new ol.style.Style({
            fill: new ol.style.Fill({
               color: 'rgba(40, 255, 40, 0.6)',
           }),
           stroke: new ol.style.Stroke({
               color: '#319FD3',
               width: 3,
           })
    });






    // Populate the sorted unique date list dropdown select box
    $.ajax('/zika_all_reports/get_date_list',
    {
        dataType: 'json', // type of response data
        timeout: 1500,     // timeout milliseconds
        success: function (data, status, xhr) {   // success callback function
            populateDateListSelectBox(data);
            //$('p').append(data.firstName + ' ' + data.middleName + ' ' + data.lastName);
        },
        error: function (jqXhr, textStatus, errorMessage) { // error callback
            $('#debug').append('Error: ' + errorMessage);
        }
    });

//    alert(`
//      map.getProjection().code = ${map.getProjection.code}
//      map.getProjection().units = ${map.getProjection.units}
//      map.getProjection().axisOrientation = ${map.getProjection.axisOrientation}
//      map.getProjection().global = ${map.getProjection.global}
//    `);


// Week 2 end point cruft
//    const zikaReportSource = new ol.source.Vector({
//        format: new ol.format.GeoJSON(),
//        url: '/zika_report/'
//    });
//    const zikaReportLayer = new ol.layer.Vector({
//        source: zikaReportSource,
//        title: "zikaReports",
//        style: function(feature) {
//
//            let numCases = feature.get('value');
//            let circleColor = 'red';
//            let sizeStep = 95000 / 50;
//            let circleRadius = 5 + numCases / sizeStep;
//            circleRadius =  (circleRadius < 5) ? 5 : circleRadius;
//            circleRadius =  (circleRadius > 55) ? 55 : circleRadius;
//            let strokeWidth = 3;
//
//            if (numCases < 1) {
//                circleRadius = 1;
//                circleColor = [40,255,40,0.3]; //RGBA: light green
//                strokeWidth = 5
//            }
//            else if (numCases < 100) {
//                circleColor = [255,120,120]; //RGB: light red
//            }
//            else if (numCases < 1000) {
//                circleColor = [255,90,90]; //RGB: light red
//            }
//            else if (numCases < 10000) {
//                circleColor = [255,50,50]; //RGB: light red
//            }
//
//            const ps = new ol.style.Style({
//                image: new ol.style.Circle({
//                    radius: circleRadius,
//                    fill: null,
//                    stroke: new ol.style.Stroke({color: circleColor, width: strokeWidth})
//                })
//            });
//            return ps;
//        }
//    });
//    map.addLayer(zikaReportLayer);

    //let routesLayers;
    //let routeName = "Test 'global' variable route name";



    let zikaLastFoundLocationsLayer = null;
    $("#reportDateListButton").click( function() {

            document.getElementById('waitSpinnerDiv').style.visibility = 'visible';

               let dateStr = $('#reportDateList').val();
               //alert("ye imperial dateStr = " + dateStr);
               const zikaFoundLocationsSource = new ol.source.Vector({
                   format: new ol.format.GeoJSON(),
                   url: `/zika_all_reports?date=${dateStr}`
               });

               let zikaFoundLocationsLayer = new ol.layer.Vector({
                   source: zikaFoundLocationsSource,
                   title: "zikaFoundLocations",
                   name: "zikaFoundLocations",
                   style: zikaFoundStatePolyStyle,
               });

               //const lyrs = map.getLayersByName("zikaFoundLocations");
               if (zikaLastFoundLocationsLayer) {
                   map.removeLayer(zikaLastFoundLocationsLayer);
               }
               zikaLastFoundLocationsLayer = zikaFoundLocationsLayer;
               map.addLayer(zikaFoundLocationsLayer);
               map.render();
               document.getElementById('fuzzySearchText').value = '';
               document.getElementById('clearReportsButton').style.visibility = 'visible';

                var listenerKey = zikaFoundLocationsSource.on('change', function(e) {
                  if (zikaFoundLocationsSource.getState() == 'ready') {
                    // hide loading icon
                    document.getElementById('waitSpinnerDiv').style.visibility = 'hidden';
                    // ...
                    // and unregister the "change" listener
                    ol.Observable.unByKey(listenerKey);
                    // or vectorSource.unByKey(listenerKey) if
                    // you don't use the current master branch
                    // of ol3
                  }
                });

           }
    );

    $("#fuzzySearchIcon").click( function() {

           let searchStr = $('#fuzzySearchText').val();
           //alert("ye imperial dateStr = " + dateStr);

            document.getElementById('waitSpinnerDiv').style.visibility = 'visible';

           const zikaFoundLocationsSource = new ol.source.Vector({
               format: new ol.format.GeoJSON(),
               url: `/api/items/search?q=${searchStr}`
           });

           let zikaFoundLocationsLayer = new ol.layer.Vector({
               source: zikaFoundLocationsSource,
               title: "zikaFoundLocations",
               name: "zikaFoundLocations",
               style: zikaFoundStatePolyStyle,
           });


           //const lyrs = map.getLayersByName("zikaFoundLocations");
           if (zikaLastFoundLocationsLayer) {
               map.removeLayer(zikaLastFoundLocationsLayer);
           }
           zikaLastFoundLocationsLayer = zikaFoundLocationsLayer;
           map.addLayer(zikaFoundLocationsLayer);
           map.render();
           document.getElementById('clearReportsButton').style.visibility = 'visible';

            var listenerKey = zikaFoundLocationsSource.on('change', function(e) {
              if (zikaFoundLocationsSource.getState() == 'ready') {
                // hide loading icon
                document.getElementById('waitSpinnerDiv').style.visibility = 'hidden';
                // ...
                // and unregister the "change" listener
                ol.Observable.unByKey(listenerKey);
                // or vectorSource.unByKey(listenerKey) if
                // you don't use the current master branch
                // of ol3
              }
            });


       }
    );

    //map.rendercomplete()
//    map.on('postrender', function(event) {
//            document.getElementById('waitSpinnerDiv').style.visibility = 'hidden';
//    });

    $("#clearReportsButton").click( function() {
           if (zikaLastFoundLocationsLayer) {
               map.removeLayer(zikaLastFoundLocationsLayer);
           }
           zikaLastFoundLocationsLayer = null;
           document.getElementById('fuzzySearchText').value = '';
           document.getElementById('clearReportsButton').style.visibility = 'hidden';
        }
    );

///////////////////
//    map.addLayer(zikaBaseLocationsLayer);
//    map.addLayer(zikaStatesGeoSvrLayer);

    let CDClocationsLayerChkBxIsOn = true;
    $("#CDClocationsLayerChkBx").click( function() {

            if (CDClocationsLayerChkBxIsOn) {
                CDClocationsLayerChkBxIsOn = false;
                //alert("checkbox set to false");
                map.removeLayer(zikaBaseLocationsLayer);
                document.getElementById('CDClocDiv').style.fontWeight = 'normal';
            }
            else {
                CDClocationsLayerChkBxIsOn = true;
                //alert("checkbox set to true");
                map.addLayer(zikaBaseLocationsLayer);
                document.getElementById('CDClocDiv').style.fontWeight = 'bold';
            }
            map.render();
        }
    );


    let GeoSvrStatesLayerChkBxIsOn = true;
    $("#GeoSvrStatesLayerChkBx").click( function() {

            if (GeoSvrStatesLayerChkBxIsOn) {
                GeoSvrStatesLayerChkBxIsOn = false;
                //alert("checkbox set to false");
                map.removeLayer(zikaStatesGeoSvrLayer);
                document.getElementById('GeoSvrDiv').style.fontWeight = 'normal';
            }
            else {
                GeoSvrStatesLayerChkBxIsOn = true;
                //alert("checkbox set to true");
                map.addLayer(zikaStatesGeoSvrLayer);
                document.getElementById('GeoSvrDiv').style.fontWeight = 'bold';
            }
            map.render();
        }
    );


//////////////////////////////////////////
// MAP ON CLICK EVENT
//////////////////////////////////////////
    map.on('click', function(event) {
        let reports = [];
        let CDClocationReport = [];
        map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {


            if (! buildClickedReports(reports, feature)) {
               buildClickedCDClocationReport(CDClocationReport, feature);
            }


        });
        //////////////////////////////////////////
        // END map.forEachFeatureAtPixel() loop
        //////////////////////////////////////////

        if (reports.length < 1) {


            document.getElementById('zika_reports').innerHTML = "Loading... please wait...";
            var view = map.getView();
            var viewResolution = view.getResolution();
            var source = zikaStatesGeoSvrLayer.getSource();
            var url = source.getGetFeatureInfoUrl(
              event.coordinate, viewResolution, view.getProjection(),
              {'INFO_FORMAT': 'text/html', 'FEATURE_COUNT': 500});
            if (url) {
//              document.getElementById('zika_reports').innerHTML =  '<iframe seamless src="' + url + '"></iframe>';
                $.ajax(url,
                {
                    dataType: 'html', // type of response data
                    timeout: 1500,     // timeout milliseconds
                    success: function (data, status, xhr) {   // success callback function
                        if (data.search("<table class=") > 1 && GeoSvrStatesLayerChkBxIsOn) {
                            document.getElementById('zika_reports').innerHTML = data;
                            dialog.dialog({title: `GeoServer WMS Layer GetFeatureInfo:`});
                            dialog.dialog( "open" );
                        }
                        else {
                            if (CDClocationReport.length > 0 && CDClocationsLayerChkBxIsOn) {
                                displayClickedCDClocationReport(CDClocationReport, dialog);
                            }
                            else {
                                displayClickedReports(reports, dialog);
                            }
                        }
                        //$('p').append(data.firstName + ' ' + data.middleName + ' ' + data.lastName);
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        document.getElementById('zika_reports').innerHTML = errorMessage;
                        dialog.dialog({title: `ERROR: GeoServer WMS Layer GetFeatureInfo failed:`});
                        dialog.dialog( "open" );
                    }
                });
            }
            else {
                displayClickedReports(reports, dialog);
            }
        }
        else {
            displayClickedReports(reports, dialog);
        }


    });
    //////////////////////////////////////////
    // MAP ON CLICK EVENT END
    //////////////////////////////////////////

    /*
    map.on('click', function(event) {
        //$('#airports').empty();

        let layersToRemove = [];
        map.getLayers().forEach(function (layer) {
            if (layer.get('title') != undefined && layer.get('title') === 'route') {
                layersToRemove.push(layer);
            }
        });
        for(var i = 0; i < layersToRemove.length; i++) {
            map.removeLayer(layersToRemove[i]);
        }



        map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
               //console.log("Route name = '" + routeName + "'");
               //console.log("Layer title = '" + layer.get('title') + "'");
               //console.log("Feature name = '" + feature.get('name') + "'");
               //console.log("Airport ID = '" + feature.get('airportId') + "'");
               if (layer.get('title') === "airports") {
                    const airportId = feature.get('airportId');

                    let routeSource = new ol.source.Vector({
                        format: new ol.format.GeoJSON(),
                        url: '/route/?srcId=' + airportId
                    });
                    let routeLayer = new ol.layer.Vector({
                        source: routeSource,
                        title: "route"
                        //style: function(feature) {
                        //    return pointStyle;
                        //}
                    });
                    map.addLayer(routeLayer);


               }
        });

    });
    */




            document.getElementById('waitSpinnerDiv').style.visibility = 'hidden';
            //document.getElementById('waitSpinnerDiv').style.visibility = 'visible';

///////////// END DOC READY ///////////////
///////////////////////////////////////////
});